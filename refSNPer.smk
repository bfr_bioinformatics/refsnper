# Stub for pipelines that are readbases

import os
import pandas as pd
shell.executable("bash")


# definitions ---------------------------------------------------------------

# Set snakemake main workdir variable
workdir: config["workdir"]

# reference directory
ref_db = config["ref_db"] #"/cephfs/abteilung4/deneke/Projects/RefSNPer/db/Brucella_small"

# samples
samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str



# functions -----------------------------------------

def _get_fastq(wildcards,read_pair='fq1'):
    return samples.loc[(wildcards.sample), [read_pair]].dropna()[0]




# rules -----------------------------------------

refdirs, reffiles = glob_wildcards(os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.gz"))


rule all_run:
    input:
        expand(os.path.join(ref_db, "refseq/bacteria/{refdir}/{reffile}_genomic.fna.1.bt2"),zip, refdir=refdirs, reffile=reffiles), # unziped
        expand(expand("results/{{sample}}/{refdir}/{reffile}.bam", zip,  reffile = reffiles, refdir=refdirs), sample=samples.index),
        expand(expand("results/{{sample}}/{refdir}/{reffile}.bed", zip,  reffile = reffiles, refdir=refdirs), sample=samples.index),
        expand(expand("results/{{sample}}/{refdir}/{reffile}.vcf.gz", zip,  reffile = reffiles, refdir=refdirs), sample=samples.index),
        expand(expand("results/{{sample}}/{refdir}/{reffile}.summary.tsv", zip,  reffile = reffiles, refdir=refdirs), sample=samples.index),
        expand(expand("results/{{sample}}/{refdir}/{reffile}.summaryinfo.tsv", zip,  reffile = reffiles, refdir=refdirs), sample=samples.index),
        expand("results/{sample}/summary_info.tsv", sample=samples.index),
        "results/summary_bestsnp.tsv",
        "results/summary_bestcoverage.tsv",
        "results/summary_bestcombination.tsv"

rule all_createdb:
    input:
        os.path.join(ref_db,"refseq/bacteria"),
        os.path.join(ref_db,"db_summary.tsv"),

# ------------------------------------------------------------------------
# database building

rule download_reference:
    output: 
        outdir = directory(os.path.join(config["ref_db"],"refseq/bacteria"))
    message: "Downloading reference database"
    log: "logs/download.log"
    threads: 1#config["parameters"]["threads"]
    params:
        assembly_level = config["parameters"]["assembly_level"], #"complete", #all
        outformat = config["parameters"]["outformat"], #"fasta,assembly-report",
        outdir = config["ref_db"], #"/cephfs/abteilung4/deneke/Projects/RefSNPer/db/Brucella",
        dryrun = config["parameters"]["dryrun"], #"", #"--dry-run"
        extra = config["parameters"]["extra"], #"", #"--parallel 10 --human-readable"
        category = config["parameters"]["category"], #"all", # all,reference,representative
        genus = config["parameters"]["genus"], #"Brucella abortus"#
    shell:
        """
        ncbi-genome-download --genus "{params.genus}" --assembly-level {params.assembly_level} --format {params.outformat} --refseq-category {params.category} --output-folder {params.outdir} {params.extra} {params.dryrun} bacteria
        """

rule global_reference_summary:
    input: 
        ref_dir = os.path.join(config["ref_db"],"refseq/bacteria")
    output: 
        summary = os.path.join(ref_db,"db_summary.tsv")
    shell:
        """
        for path in `ls --color=never -d {input.ref_dir}/*`; do basename $path >> {output.summary}; done
        """

#rule reference_summary:
    #input: 
        #reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.gz"),
        #reference_report = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_assembly_report.txt")
    #output: 
        #summary = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_summary.csv")
    #shell:
        #"""
        #Assembly_name=`grep 'Assembly name:' --color=never {input.reference_report} | awk -F 'Assembly name:' '{{print $2}}' | sed -E 's/^[ ]+//'`
        #Organism_name=`grep 'Organism name:' --color=never {input.reference_report} | awk -F 'Organism name:' '{{print $2}}' | sed -E 's/^[ ]+//'`
        #Species_name=`echo $Organism_name | cut -f 1-2 -d ' '`
        #Infraspecific_name=`grep 'Infraspecific name:' --color=never {input.reference_report} | awk -F 'Infraspecific name:' '{{print $2}}' | sed -E 's/^[ ]+//'`
        #Taxid=`grep 'Taxid:' --color=never {input.reference_report} | awk -F 'Taxid:' '{{print $2}}'  | sed -E 's/^[ ]+//'`
        #Assembly_level=`grep 'Assembly level:' --color=never {input.reference_report} | awk -F 'Assembly level:' '{{print $2}}' | sed -E 's/^[ ]+//'`
        #Genome_representation=`grep 'Genome representation:' --color=never {input.reference_report} | awk -F 'Genome representation:' '{{print $2}}' | sed -E 's/^[ ]+//'`
        
         #echo "${{Assembly_name}};${{Organism_name}};${{Species_name}};${{Infraspecific_name}};${{Taxid}};${{Assembly_level}};${{Genome_representation}};{input.reference_file}" | tee {output.summary}
        #"""
#TODO fix parsing problems
# TODO convert to json, search for json parser

# 
#"""
## Assembly name:  ASM171542v1                                                                                                                                                                                                                
## Organism name:  Brucella suis (a-proteobacteria)                                                                                                                                                                                           
## Infraspecific name:  strain=2004000577                                                                                                                                                                                                     
## Taxid:          29461                                                                                                                                                                                                                      
## BioSample:      SAMN05559129                                                                                                    
## BioProject:     PRJNA338468                                                                                                     
## Submitter:      Viginia Tech                                                                                                            
## Date:           2016-8-29
## Assembly type:  n/a
## Release type:   major
## Assembly level: Complete Genome
## Genome representation: full
## Assembly method: Newbler v. 2.9
## Reference guided assembly: CP000872.1
#"""


#TODO rule check_db:

# execution part ----------------------------------------------------------------------------------------------------------------------------------

rule unzip_reference:
    input: 
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.gz")
    output: 
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna")
    message: "Unzipping reference {wildcards.refdir}"
    threads: 1 #config["parameters"]["threads"]
    shell:
        """
        gunzip -k {input.reference_file}
        """

rule index_reference:
    input: 
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna")
    output: 
        reference_index = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.1.bt2")
    message: "Indexing reference {wildcards.refdir}"
    #log: "logs/fastp_{sample}.log"
    threads: config["parameters"]["threads"]
    shell:
        """
        echo "bowtie2-build {input.reference_file} {input.reference_file}"
        bowtie2-build {input.reference_file} {input.reference_file}
        """


rule map_reads:
    input:
        fq1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        fq2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna"),
        reference_index = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.1.bt2")
    output:
        bam = "results/{sample}/{refdir}/{reffile}.bam"
    message: "Running bowtie2 mapping on {wildcards.sample} and reference {wildcards.refdir} {wildcards.reffile}"
    log: "logs/bowtie2_{sample}_{refdir}_{reffile}.log"
    threads: config["parameters"]["threads"]
    params:
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna.gz")
    shell:
        """
        echo "bowtie2 -1 {input.fq1} -2 {input.fq2} -q --sensitive --no-unal -a -x {input.reference_file} --threads {threads} | samtools sort - -o {output.bam} &> {log}"
        bowtie2 -1 {input.fq1} -2 {input.fq2} -q --sensitive --no-unal -a -x {input.reference_file} --threads {threads} | samtools sort - -o {output.bam} &> {log}
        """



rule run_bedtools:
    input:
        bam = "results/{sample}/{refdir}/{reffile}.bam"
    output:
        bed = "results/{sample}/{refdir}/{reffile}.bed"
    message: "Running bedtools on {wildcards.sample} and reference {wildcards.refdir} {wildcards.reffile}"
    log: "logs/bedtools_{sample}_{refdir}_{reffile}.log"
    threads: config["parameters"]["threads"]
    shell:
        """
        echo "samtools view -b {input.bam} | genomeCoverageBed -ibam stdin -d > {output.bed} 2> {log}"
        samtools view -b {input.bam} | genomeCoverageBed -ibam stdin -d > {output.bed} 2> {log}
        """


rule run_bcftools:
    input:
        bam = "results/{sample}/{refdir}/{reffile}.bam",
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna")
    output:
        vcf = "results/{sample}/{refdir}/{reffile}.vcf.gz"
    message: "Running bcftools on {wildcards.sample} and reference {wildcards.refdir} {wildcards.reffile}"
    log: "logs/bcftools_{sample}_{refdir}_{reffile}.log"
    threads: config["parameters"]["threads"]
    shell:
        """
        echo "bcftools mpileup --threads {threads} -Ou -f {input.reference_file} {input.bam} | bcftools call -mv -Oz -o {output.vcf} --threads {threads} --ploidy 1 - 2> {log}"
        bcftools mpileup --threads {threads} -Ou -f {input.reference_file} {input.bam} | bcftools call -mv -Oz -o {output.vcf} --threads {threads} --ploidy 1 - 2> {log}
        """


rule parse_bedtools_bcftools:
    input:
        bam = "results/{sample}/{refdir}/{reffile}.bam",
        bed = "results/{sample}/{refdir}/{reffile}.bed",
        reference_file = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_genomic.fna"),
        vcf = "results/{sample}/{refdir}/{reffile}.vcf.gz",
    output:
        stats = "results/{sample}/{refdir}/{reffile}.summary.tsv"
    message: "Sumamry on {wildcards.sample} and reference {wildcards.refdir} {wildcards.reffile}"
    log: "logs/summary_{sample}_{refdir}_{reffile}.log"
    threads: config["parameters"]["threads"]
    shell:
        """
        reads=`samtools view {input.bam} | wc -l`
        coveredbases=`awk '$3 > 0' {input.bed} | wc -l`
        averagedepth=`awk '{{sum=sum+$3}}; END{{print sum/NR}}' {input.bed}`
        ref_length=`wc -l {input.bed} | cut -f 1 -d ' '`
        coverage=`awk '{{if($3 > 0) basecount=basecount+1}}; END{{print basecount/NR}}' {input.bed}`
        SNPcount=`bcftools view {input.vcf} | grep -v 'INDEL' | grep -v -c '^#' || echo | tr -d '\n'`
        SNPdensity=`bc -l <<< $SNPcount/$coveredbases`
        # coverage_below_threshold=`awk -v coverage="$coverage" 'BEGIN {{if(coverage < 0.9) print "TRUE"; else print "FALSE"}}'`
        # if [[ $SNPdensity == 0 && $coverage_below_threshold == "TRUE" ]]; then SNPdensity="NA"; fi # decide dynamically whether SNP information is relevant
        
        echo -e "#sample\treference\treads\tcoveredbases\tcoverage\taveragedepth\tSNPcount\tSNPdensity" > {output.stats}
        echo -e "{wildcards.sample}\t{wildcards.refdir}\t$reads\t$coveredbases\t$coverage\t$averagedepth\t$SNPcount\t$SNPdensity" >> {output.stats}
        """


rule merge_stats_refinfo:
    input:
        stats = "results/{sample}/{refdir}/{reffile}.summary.tsv",
        reference_report = os.path.join(ref_db,"refseq/bacteria/{refdir}/{reffile}_assembly_report.txt")
    output:
        stats = "results/{sample}/{refdir}/{reffile}.summaryinfo.tsv"
    message: "Merging sample stats and reference info for sample {wildcards.sample} and  reference {wildcards.refdir} {wildcards.reffile}"
    log: "logs/summaryinfo_{sample}_{refdir}_{reffile}.log"
    script:
        "scripts/parse_stats.R"




rule merge_per_sample:
    input:
        stats = expand("results/{{sample}}/{refdir}/{reffile}.summaryinfo.tsv",zip, refdir=refdirs, reffile=reffiles),
    output:
        summary_tsv = "results/{sample}/summary_info.tsv",
        bestsnp_tsv = "results/{sample}/summary_bestsnp.tsv",
        bestcoverage_tsv = "results/{sample}/summary_bestcoverage.tsv",
        bestcombo_tsv = "results/{sample}/summary_bestcombination.tsv",
    message: "Merging summary and refinfo for all references for sample {wildcards.sample}"
    script:
        "scripts/sample_summary.R"


# summary over all samples

rule merge_best_SNP:
    input:
        bestsnp_tsv = expand("results/{sample}/summary_bestsnp.tsv",sample=samples.index)
    output:
        bestsnp_tsv = "results/summary_bestsnp.tsv"
    message: "Merging summary and refinfo for all references and all samples: best SNP result"
    shell:
        """
        head -n 1 {input.bestsnp_tsv[0]} > {output.bestsnp_tsv}
        for file in {input.bestsnp_tsv}; do tail -n +2  $file >> {output.bestsnp_tsv}; done
        """

rule merge_best_coverage:
    input:
        bestcoverage_tsv = expand("results/{sample}/summary_bestcoverage.tsv",sample=samples.index)
    output:
        bestcoverage_tsv = "results/summary_bestcoverage.tsv"
    message: "Merging summary and refinfo for all references and all samples: best SNP result"
    shell:
        """
        head -n 1 {input.bestcoverage_tsv[0]} > {output.bestcoverage_tsv}
        for file in {input.bestcoverage_tsv}; do tail -n +2  $file >> {output.bestcoverage_tsv}; done
        """

rule merge_best_combo:
    input:
        bestcombo_tsv = expand("results/{sample}/summary_bestcombination.tsv",sample=samples.index)
    output:
        bestcombo_tsv = "results/summary_bestcombination.tsv"
    message: "Merging summary and refinfo for all references and all samples: best SNP result"
    shell:
        """
        head -n 1 {input.bestcombo_tsv[0]} > {output.bestcombo_tsv}
        for file in {input.bestcombo_tsv}; do tail -n +2  $file >> {output.bestcombo_tsv}; done
        """




#more ------------------------------------------------------------------------------

##TODO visualize contigs

#rule vizualize_contigs:
    #input:
        #bam
        #bed
        #reference_file
        #vcf
    #output:
        #pdf = 
    #message: "Running bowtie2 mapping on {wildcards.sample} and reference xy"
    ##conda:
    ##   "envs/fastp.yaml"
    #log: "logs/fastp_{sample}.log"
#shell:
    #"""
        #echo "/usr/bin/Rscript /home/DenekeC/Rscripts/Coverage_from_bedfile.R -i $bedout" | tee -a {log}
        #/usr/bin/Rscript /home/DenekeC/Rscripts/Coverage_from_bedfile.R -i $bedout -o $pdfout 2>&1  | tee -a {log} || true
    #"""


#TODO write report
#rule qc_html_report:
    #input:
        #workdir = config["workdir"],
        #stats = "fastp_summary.tsv",
    #output:
        #"fastp_summary.html"
    #conda:
        #"envs/rmarkdown_env.yaml"
    #message: "Creating QC Rmarkdown report"
    #script:
        #"scripts/write_QC_report.Rmd"
