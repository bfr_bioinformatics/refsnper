# refSNPer: A Snakemake workflow for the identification of the closest reference strains

The closest reference strain to a given input sequence is inferred by sequential comparison to a set of reference sequences. The closest reference strain may be the strain that is best covered or with the fewest SNPs.
Which criterion (coverage vs. SNPs) is more relevant depends on the sequencing depth (low depth -> prefer coverage) and the reference heterogeneity (closely related references -> prefer SNPs).


Input can be either a set of isolate or metagenomic sequences (provided as paired fastq files).

The main steps involve
* mapping of each input sample to each of the references using bowtie2
* identification of the coverage and coverage depth of each reference using bedtools
* identification of putative SNPs to each reference using bcftools

### Overview

![Overview of workflow](RefSNPer_workflow.png) { width: 100px; }

The input (a set of isolate or metagenomic sequences provided as paired fastq files) is mapped to a reference database. The reference database can be automatically generated for user-defined genera or species from draft and/or complete genome sequences available at the NCBI RefSeq database. For each reference genome the coverage and coverage depth (bedtools) as well as the number of putative SNPs (bcftools) are determined. A summary file is generated that outputs the SNP density for each reference genome.


## Usage

### Step 1: Install workflow

Clone this repository

    cd /path/to/installation
    git clone https://gitlab.com/bfr_bioinformatics/refsnper.git


Install all dependencies using conda

    cd /path/to/installation
    conda env create -n refsnper --file=refsnper/envs/refsnper.yaml


For more information about conda,  see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html). 



### Step 2: Configure workflow

Configure the workflow according to your needs via editing the file `config_refsnper.yaml`.

In particular, specify
* workdir The path where all output is written to
* samples A path to a sample sheet with all fastq files as tsv in the format
* ref_db Path to reference database (will be downloaded or used for workflow execution)
* genus Name of genus or species to be downloaded as reference set. A comma-seperated list of genera is also possible. For example: "Streptomyces coelicolor,Escherichia coli".
* assembly_level Eithter "complete" or "all"
* category: Either "all" ,"reference" or "representative"

For more dvetails about the options for downloading references, type

    ncbi-genome-download --help

(with active conda environment)

If you want to create a new reference database, the choice of workdir and sample has no effect
If you want to execute a new sample set on an existing reference database, the choice of genus, assembly_level and category has no effect


#### Create a sample sheet


All data must be supplied in a sample sheet (samples.tsv). It requires the following entries:

```
sample    fq1   fq2
sample_1    path/2/sample1_R1.fastq.gz  path/2/sample1_R2.fastq.gz
sample_2    path/2/sample2_R1.fastq.gz  path/2/sample2_R2.fastq.gz
```

The sample sheet can be created with the helper script `/path/to/installation/refsnper/scripts/create_sampleSheet.sh`.

```
cd path/2/fastqdata
path/2/repo/scripts/create_sampleSheet.sh --mode flex ## using a flexible scheme where the part before the first '_' denotes the sample name sample_*_R[12]_*.fastq.gz
path/2/repo/scripts/create_sampleSheet.sh --mode illumina ## using the illulima naming scheme sample_S[0-9]_L001_R[12]_001.fastq.gz
path/2/repo/scripts/create_sampleSheet.sh --mode ncbi ## using ncbi style names: sample_[12].fastq.gz
```

See `path/2/repo/scripts/create_sampleSheet.sh --help` for more details.

If you use not standard sample names, e.g. containing "_", you may have to fill out a sample sheet manually.




### Step 3: Create reference database

Activate your conda environment

    conda activate refsnper
    cd /path/to/installation/refsnper

Executute the database execution

    snakemake --snakefile refSNPer.smk --configfile config_refsnper.yaml -f all_createdb


All NCBI refseq genomes matching the criteria genus, assembly_level, category will be automatically downloaded to `{ref_db}/refseq/bacteria` using the tool [ncbi-genome-download](https://github.com/kblin/ncbi-genome-download).

A summary of all accessions is given in file `ref_db/db_summary.tsv`

You may also execute the reference download sequentially with different search criteria, e.g. first with "reference" followed by "representative".

Note that choosing loose criteria may result in the download of 1000s of genomes. This will slow down the later execution significantly. It may be advisable to start with a restricted search on genus level and a category followed be a second run on a specific species.



#### Adding custom references to the database
You can also add custom references not found on ncbi as reference files. Make sure to place each extra reference in an individual directory below `{ref_db}/refseq/bacteria`. Each reference must contain files
* {myname}_assembly_report.txt
* {myname}_genomic.fna.gz

Make sure to assign a unique value for {myname} and that the file {myname}_assembly_report.txt contains the entries specified in the file `dummy_assembly_report.txt`:
* \# Assembly name:  ENTER_VALUE
* \# Organism name:  ENTER_VALUE
* \# Infraspecific name:  strain=ENTER_VALUE
* \# Taxid:          ENTER_VALUE
* \# Assembly level: ENTER_VALUE
* \# Genome representation: ENTER_VALUE


### Step 4: Execute workflow


Activate your conda environment

    conda activate refsnper
    /path/to/installation/refsnper

Test your configuration by performing a dry-run via

    snakemake --snakefile refSNPer.smk --configfile config_refsnper.yaml -f all_run --dryrun

Execute the workflow locally via

    snakemake --snakefile refSNPer.smk --configfile config_refsnper.yaml -f all_run --cores $N

using `$N` cores

See the [Snakemake documentation](https://snakemake.readthedocs.io) for further details.


## Output description



## Authors

* Carlus Deneke (@deneke)


## References
* [Snakemake documentation](https://snakemake.readthedocs.io)
* [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html). 
* [ncbi-genome-download](https://github.com/kblin/ncbi-genome-download)
* [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* [bedtools](https://bedtools.readthedocs.io/en/latest/)
* [bcftools](http://samtools.github.io/bcftools/bcftools.html)
